
package dao;

import entity.Category;
import entity.Product;
import entity.SubCategory;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import util.HibernateUtil;

public class ListDao 
{
    public List catList() 
    {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        
        List<Category> cList = session.createQuery("SELECT al.catName FROM Category al").list();
        cList.toString();
        
        session.close();
        return cList;
    }
    // finish
    
    
    
    
    
    
    public List subcatList(String name) 
    {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        
        List<Category> cList = session.createQuery
        ("SELECT al.subCatName FROM SubCategory al where al.category.catId IN (SELECT a.catId FROM Category a where lower(a.catName)='"+ name.toLowerCase() + "')").list();
        cList.toString();
        
        session.close();
        return cList;
    }
    // finish
    
    
    
    
    
    
    // CatList by name
    public List<Category> catListByName(String name)
    {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        
        List<Category> cList = session.createQuery
        ("SELECT al FROM Category al where lower(catName) = '"+ name.toLowerCase() +"'").list();
        cList.toString();
        
        session.close();
        return cList;
    } 
    
    
    
    
    // SubCatList by name
    public List<SubCategory> subcatListByName(String name)
    {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        
        List<SubCategory> cList = session.createQuery
        ("SELECT al FROM SubCategory al where lower(subCatName) = '"+ name.toLowerCase() +"'").list();
        cList.toString();
        
        session.close();
        return cList;
    } 
    // finish
    
    
    
    
    public List allProductList() 
    {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        
        List<Product> cList = session.createQuery("SELECT al FROM Product al").list();
        cList.toString();
        
        session.close();
        return cList;
    }
    // finish
} 
