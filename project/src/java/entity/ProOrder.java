package entity;
// Generated 05-Mar-2018 09:37:22 by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * ProOrder generated by hbm2java
 */
public class ProOrder  implements java.io.Serializable {


     private Integer odrId;
     private BookUser bookUser;
     private DeliveryCost deliveryCost;
     private Product product;
     private Date odrDate;
     private int odrQty;
     private double totalPrice;
     private String odrStatus;
     private Set proOdrDetailses = new HashSet(0);

    public ProOrder() {
    }

	
    public ProOrder(BookUser bookUser, DeliveryCost deliveryCost, Product product, Date odrDate, int odrQty, double totalPrice, String odrStatus) {
        this.bookUser = bookUser;
        this.deliveryCost = deliveryCost;
        this.product = product;
        this.odrDate = odrDate;
        this.odrQty = odrQty;
        this.totalPrice = totalPrice;
        this.odrStatus = odrStatus;
    }
    public ProOrder(BookUser bookUser, DeliveryCost deliveryCost, Product product, Date odrDate, int odrQty, double totalPrice, String odrStatus, Set proOdrDetailses) {
       this.bookUser = bookUser;
       this.deliveryCost = deliveryCost;
       this.product = product;
       this.odrDate = odrDate;
       this.odrQty = odrQty;
       this.totalPrice = totalPrice;
       this.odrStatus = odrStatus;
       this.proOdrDetailses = proOdrDetailses;
    }
   
    public Integer getOdrId() {
        return this.odrId;
    }
    
    public void setOdrId(Integer odrId) {
        this.odrId = odrId;
    }
    public BookUser getBookUser() {
        return this.bookUser;
    }
    
    public void setBookUser(BookUser bookUser) {
        this.bookUser = bookUser;
    }
    public DeliveryCost getDeliveryCost() {
        return this.deliveryCost;
    }
    
    public void setDeliveryCost(DeliveryCost deliveryCost) {
        this.deliveryCost = deliveryCost;
    }
    public Product getProduct() {
        return this.product;
    }
    
    public void setProduct(Product product) {
        this.product = product;
    }
    public Date getOdrDate() {
        return this.odrDate;
    }
    
    public void setOdrDate(Date odrDate) {
        this.odrDate = odrDate;
    }
    public int getOdrQty() {
        return this.odrQty;
    }
    
    public void setOdrQty(int odrQty) {
        this.odrQty = odrQty;
    }
    public double getTotalPrice() {
        return this.totalPrice;
    }
    
    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }
    public String getOdrStatus() {
        return this.odrStatus;
    }
    
    public void setOdrStatus(String odrStatus) {
        this.odrStatus = odrStatus;
    }
    public Set getProOdrDetailses() {
        return this.proOdrDetailses;
    }
    
    public void setProOdrDetailses(Set proOdrDetailses) {
        this.proOdrDetailses = proOdrDetailses;
    }




}


