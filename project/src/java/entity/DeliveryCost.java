package entity;
// Generated 05-Mar-2018 09:37:22 by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;

/**
 * DeliveryCost generated by hbm2java
 */
public class DeliveryCost  implements java.io.Serializable {


     private Integer delId;
     private String zoneName;
     private double delCost;
     private Set proOrders = new HashSet(0);

    public DeliveryCost() {
    }

	
    public DeliveryCost(String zoneName, double delCost) {
        this.zoneName = zoneName;
        this.delCost = delCost;
    }
    public DeliveryCost(String zoneName, double delCost, Set proOrders) {
       this.zoneName = zoneName;
       this.delCost = delCost;
       this.proOrders = proOrders;
    }
   
    public Integer getDelId() {
        return this.delId;
    }
    
    public void setDelId(Integer delId) {
        this.delId = delId;
    }
    public String getZoneName() {
        return this.zoneName;
    }
    
    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }
    public double getDelCost() {
        return this.delCost;
    }
    
    public void setDelCost(double delCost) {
        this.delCost = delCost;
    }
    public Set getProOrders() {
        return this.proOrders;
    }
    
    public void setProOrders(Set proOrders) {
        this.proOrders = proOrders;
    }




}


